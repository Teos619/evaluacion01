/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.evaluacion;

import java.util.Scanner;

/**
 *
 * @author luist
 */
public class TestUsuario {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Usuario miUsuario = new Usuario();
        
        String inputUser, inputPassword;
        boolean intentos = false;
        
        
        
        System.out.println("--FORMULARIO DE REGISTRO--");
        
 
        System.out.println("Digite su nombre: ");
        miUsuario.setNombre(in.nextLine());
        
        System.out.println("Digite su usuario: ");
        miUsuario.setUsuario(in.nextLine());
        
        System.out.println("Digite su Nueva Contraseña: ");
        miUsuario.setPassword(in.nextLine());
        
        /* password ingresado*/
        miUsuario.setPasswordIngresado(miUsuario.getPassword());
        
        System.out.println("Digite su Correo: ");
        miUsuario.setCorreo(in.nextLine());
        
        
         /*Luego de Registro Iniciar Sesion...*/
        System.out.println("--DATOS INGRESADOS, AHORA INICIE SESION--");
        System.out.println("-------------------");
          
        /*que siga preguntando hasta que de con el usuariuo y contraseña*/
          while (!intentos) {
            System.out.println("Usuario: ");
            inputUser = in.nextLine();

            System.out.println("Contraseña: ");
            inputPassword = in.nextLine();

            /*Probamos iniciar sesion...*/
            if (miUsuario.iniciarSesion(inputUser, inputPassword)) {
                miUsuario.mostrarDatos();
                
                intentos = true; /*salimos del while*/
                
            } else {
                System.out.println("--ACCESO DENEGADO INTENTE DE NUEVO--");
                System.out.println("-------------------");
                intentos = false;
            }

        }


        
        
        
        
        
       
       
        
        
        
        
    }
    
}
