/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.evaluacion;

/**
 *
 * @author luist
 */
public class Usuario {
   
    private String nombre;
    private String usuario;
    private String password;
    private String passwordIngresado;
    private String correo;
    
    public Usuario(){
    }

    public Usuario(String nombre, String usuario, String password) {
        this.nombre = nombre;
        this.usuario = usuario;
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordIngresado() {
        return passwordIngresado;
    }

    public void setPasswordIngresado(String passwordIngresado) {
        this.passwordIngresado = passwordIngresado;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    
    public boolean iniciarSesion(String user, String pass ){
        
        if(user.equals(this.usuario) && pass.equals(this.password)){
         System.out.println("--Bienvenido!--");
         return true;
         
        }else{
         System.out.println("--Datos Erroneos--");
         return false;
        }
   
    
    }
    
    public void mostrarDatos(){
     System.out.println("Nombre: "+getNombre());
     System.out.println("Usuario: "+getUsuario());
     System.out.println("Contraseña: "+getPassword());
     System.out.println("Contraseña Ingresada: "+getPasswordIngresado());
     System.out.println("Correo: "+getCorreo());
          
  
    }
    
  
    
}
